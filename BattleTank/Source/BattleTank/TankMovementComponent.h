// Copyright notice.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/NavMovementComponent.h"
#include "TankMovementComponent.generated.h"

class UTankTrack;

/**
 * Handles the tank's movement
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankMovementComponent : public UNavMovementComponent
{
	GENERATED_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = "Input")
	void Initialize(UTankTrack* leftTrack, UTankTrack* rightTrack);

	UFUNCTION(BlueprintCallable, Category = "Input")
	void IntendMoveForward(float Throw);
	
	UFUNCTION(BlueprintCallable, Category = "Input")
	void IntendMoveBackward(float Pull);

	UFUNCTION(BlueprintCallable, Category = "Input")
	void IntendTurnLeft(float magnitude);

	UFUNCTION(BlueprintCallable, Category = "Input")
	void IntendTurnRight(float magnitude);

private:
	
	// Invoked by the path finding logic by the AI controller
	void RequestDirectMove(const FVector& MoveVelocity, bool bForceMaxSpeed) override;

	UTankTrack* LeftTrack = nullptr;

	UTankTrack* RightTrack = nullptr;
};
