// Copyright notice

#include "SprungWheel.h"
#include "Components/SphereComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"

// Sets default values
ASprungWheel::ASprungWheel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.TickGroup = TG_PostPhysics;

	MassAxleSpring = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Mass Axle Spring"));
	SetRootComponent(MassAxleSpring);

	Axle = CreateDefaultSubobject<USphereComponent>(FName("Axle"));
	Axle->SetupAttachment(MassAxleSpring);

	Wheel = CreateDefaultSubobject<USphereComponent>(FName("Wheel"));
	Wheel->SetupAttachment(Axle);

	AxleWheelSpring = CreateDefaultSubobject<UPhysicsConstraintComponent>(FName("Axle Wheel Spring"));
	AxleWheelSpring->SetupAttachment(Axle);
}

// Called when the game starts or when spawned
void ASprungWheel::BeginPlay()
{
	Super::BeginPlay();

	SetupConstraints();

	Wheel->SetNotifyRigidBodyCollision(true);
	Wheel->OnComponentHit.AddDynamic(this, &ASprungWheel::OnHit);
}

void ASprungWheel::SetupConstraints()
{
	AActor* parentActor = GetAttachParentActor();

	if (!ensure(parentActor))
		return;

	UPrimitiveComponent* parentRoot = Cast<UPrimitiveComponent>(parentActor->GetRootComponent());

	if (!ensure(parentRoot))
		return;

	MassAxleSpring->SetConstrainedComponents(parentRoot, NAME_None, Axle, NAME_None);
	AxleWheelSpring->SetConstrainedComponents(Axle, NAME_None, Wheel, NAME_None);
}

// Called every frame
void ASprungWheel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (GetWorld()->TickGroup != TG_PostPhysics)
		return;

	ForceThisFrame = 0.0f;
}

void ASprungWheel::AddDrivingForce(float ForceMagnitude)
{
	ForceThisFrame += ForceMagnitude;
}

void ASprungWheel::OnHit(UPrimitiveComponent * HitComponent, AActor * OtherActor, UPrimitiveComponent * OtherComp, FVector NormalImpulse, const FHitResult & Hit)
{
	ApplyForce();
}

void ASprungWheel::ApplyForce()
{
	Wheel->AddForce(Axle->GetForwardVector() * ForceThisFrame);
}

