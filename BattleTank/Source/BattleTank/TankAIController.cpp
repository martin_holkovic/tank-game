// Copyright notice

#include "TankAIController.h"
#include "Engine/World.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "TankPawn.h"

ATankAIController::ATankAIController()
{
	PrimaryActorTick.bCanEverTick = true;
}

void ATankAIController::BeginPlay()
{
	Super::BeginPlay();
}

void ATankAIController::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	APawn* playerTank = GetWorld()->GetFirstPlayerController()->GetPawn();
	
	// spectator only - ignore
	if (playerTank == nullptr)
		return;

	MoveToActor(playerTank, AcceptanceRadius);

	UTankAimingComponent* aimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	
	if (!ensure(aimingComponent))
		return;
	
	FVector LocPlayerTank = playerTank->GetActorLocation();

	aimingComponent->AimAtLocation(LocPlayerTank);

	if (aimingComponent->GetAimState() == EAimState::Locked)
		aimingComponent->Fire();
}

void ATankAIController::SetPawn(APawn * InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn == nullptr)
		return;

	ATankPawn* tank = Cast<ATankPawn>(InPawn);

	if (!ensure(tank))
		return;

	tank->OnDeath.AddUniqueDynamic(this, &ATankAIController::OnTankDeath);
}

void ATankAIController::OnTankDeath()
{
	GetPawn()->DetachFromControllerPendingDestroy();
}
