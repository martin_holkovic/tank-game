// Copyright notice.

#include "TankMovementComponent.h"
#include "TankTrack.h"
#include "GameFramework/Actor.h"

void UTankMovementComponent::IntendMoveForward(float Throw)
{
	if (!ensure(LeftTrack && RightTrack))
		return;

	LeftTrack->SetThrottle(Throw);
	RightTrack->SetThrottle(Throw);
}

void UTankMovementComponent::IntendMoveBackward(float Pull)
{
	if (!ensure(LeftTrack && RightTrack))
		return;

	LeftTrack->SetThrottle(Pull);
	RightTrack->SetThrottle(Pull);
}

void UTankMovementComponent::IntendTurnLeft(float magnitude)
{
	if (!ensure(LeftTrack && RightTrack))
		return;

	LeftTrack->SetThrottle(magnitude);
	RightTrack->SetThrottle(-magnitude);
}

void UTankMovementComponent::IntendTurnRight(float magnitude)
{
	if (!ensure(LeftTrack && RightTrack))
		return;

	LeftTrack->SetThrottle(magnitude);
	RightTrack->SetThrottle(-magnitude);
}

void UTankMovementComponent::RequestDirectMove(const FVector & MoveVelocity, bool bForceMaxSpeed)
{
	FVector TankForward = GetOwner()->GetActorForwardVector().GetSafeNormal();
	FVector AIForwardIntention = MoveVelocity.GetSafeNormal();

	float forwardForce = FVector::DotProduct(TankForward, AIForwardIntention);
	float sideForce = FVector::CrossProduct(TankForward, AIForwardIntention).Z;

	IntendTurnRight(sideForce);
	IntendMoveForward(forwardForce);
}

void UTankMovementComponent::Initialize(UTankTrack* leftTrack, UTankTrack* rightTrack)
{
	if (!ensure(leftTrack && rightTrack))
		return;

	LeftTrack = leftTrack;
	RightTrack = rightTrack;
}