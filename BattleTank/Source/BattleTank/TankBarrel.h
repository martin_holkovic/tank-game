// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:

	// Elevates the barrel by a relative amount of speed
	void Elevate(float RelativeSpeed);

private:

	// maximum amount of barrel yaw rotation in degrees per second
	UPROPERTY(EditAnywhere, Category=Setup)
	float MaxDegreesPerSecond = 5.0f;
	
	// lower barrel pitch rotation bound
	UPROPERTY(EditAnywhere, Category=Setup)
	float MinElevationDegrees = -5.0f;
	
	// upper barrel pitch rotation bound
	UPROPERTY(EditAnywhere, Category=Setup)
	float MaxElevationDegrees = 25.0f;
};
