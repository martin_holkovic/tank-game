// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTurret.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:

	// Turns tanks turret by a speed in a direction defined by sign of speed (- = left, + = right)
	void Turn(float RelativeSpeed);
	
private:

	UPROPERTY(EditAnywhere, Category=Setup)
	float MaxDegreesPerSecond = 25.0f;
};
