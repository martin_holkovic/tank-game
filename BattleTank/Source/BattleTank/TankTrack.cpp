// Copyright notice

#include "TankTrack.h"
#include "Components/PrimitiveComponent.h"
#include "GameFramework/Actor.h"
#include "SprungWheel.h"
#include "SpawningComponent.h"

UTankTrack::UTankTrack()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankTrack::SetThrottle(float magnitude)
{
	float newThrottle = FMath::Clamp<float>(magnitude, -1, 1);

	DriveTrack(newThrottle);
}

void UTankTrack::DriveTrack(float Throttle)
{
	TArray<class ASprungWheel*> Wheels = GetWheels();

	if (Wheels.Num() == 0)
		return;

	float ForceApplied = MaxDrivingForce * Throttle;
	float ForcePerWheel = ForceApplied / Wheels.Num();

	for (ASprungWheel* Wheel : Wheels)
		Wheel->AddDrivingForce(ForcePerWheel);
}

TArray<ASprungWheel*> UTankTrack::GetWheels() const
{
	auto items = TArray<ASprungWheel*>();
	
	TArray<USceneComponent*> children;
	GetChildrenComponents(true, children);

	for (auto child : children)
	{
		USpawningComponent* spawner = Cast<USpawningComponent>(child);

		if (spawner == nullptr)
			continue;

		AActor* spawnedActor = spawner->GetSpawnedActor();

		if (spawnedActor == nullptr)
			continue;

		ASprungWheel* spring = Cast<ASprungWheel>(spawnedActor);

		if (spring == nullptr)
			continue;

		items.Add(spring);
	}

	return items;
}
