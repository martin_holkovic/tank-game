// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTrack.generated.h"

class ASprungWheel;
class USceneComponent;

UCLASS(meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTrack : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:

	UTankTrack();

	// Max force per track in newtons (assuming 40t mass with 1g (10m/s) acceleration)
	UPROPERTY(EditDefaultsOnly)
	float MaxDrivingForce = 20000000.0f;

	void SetThrottle(float magnitude);

private:

	void DriveTrack(float Throttle);

	TArray<ASprungWheel*> GetWheels() const;
};
