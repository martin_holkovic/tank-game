// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TankAimingComponent.generated.h"

// enum constants for aiming/firing state
UENUM()
enum class EAimState : uint8
{
	Unknown,
	Reloading,
	Aiming,
	Locked,
	OutOfAmmo,
};

/// forward declaration
class UTankBarrel;
class UTankTurret;
class AProjectile;

// handles tank pawns aiming logic
UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API UTankAimingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UTankAimingComponent();

	void TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = Setup)
	void Initialize(UTankBarrel* barrelToSet, UTankTurret* turretToSet);

	UFUNCTION(BlueprintCallable)
	void Fire();

	void AimAtLocation(FVector TargetLocation);

	UPROPERTY(EditDefaultsOnly, Category = Firing)
	double ReloadTimeInSeconds = 1;

	UPROPERTY(EditDefaultsOnly, Category = Firing)
	int32 Ammo = 3;

	EAimState GetAimState() const;

	UFUNCTION(BlueprintCallable)
	int32 GetAmmo() const;

protected:

	UPROPERTY(BlueprintReadOnly, Category = "State")
	EAimState AimState = EAimState::Unknown;
	
	UPROPERTY(EditAnywhere, Category = Firing)
	float LaunchSpeed = 3000.0f;

private:

	bool bIsReloaded;

	bool bIsAiming;

	UTankBarrel* Barrel = nullptr;

	UTankTurret* Turret = nullptr;

	double LastTimeFired = 0;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	TSubclassOf<AProjectile> ProjectileBP;
	
	void MoveBarrel(FVector direction);
};
