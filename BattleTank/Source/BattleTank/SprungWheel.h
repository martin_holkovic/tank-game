// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SprungWheel.generated.h"

class UPhysicsConstraintComponent;
class USphereComponent;

UCLASS()
class BATTLETANK_API ASprungWheel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASprungWheel();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AddDrivingForce(float ForceMagnitude);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere, Category = Setup)
	USphereComponent* Wheel = nullptr;

	UPROPERTY(VisibleAnywhere, Category = Setup)
	USphereComponent* Axle = nullptr;

	UPROPERTY(VisibleAnywhere)
	UPhysicsConstraintComponent* MassAxleSpring = nullptr;

	UPROPERTY(VisibleAnywhere)
	UPhysicsConstraintComponent* AxleWheelSpring = nullptr;

private:
	
	float ForceThisFrame = 0.0f;

	void SetupConstraints();

	void ApplyForce();
};
