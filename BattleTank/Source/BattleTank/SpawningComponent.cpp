// Copyright notice

#include "SpawningComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"

USpawningComponent::USpawningComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void USpawningComponent::BeginPlay()
{
	Super::BeginPlay();
	
	SpawnActor();
}

void USpawningComponent::SpawnActor()
{
	if (!ensure(ActorToSpawn))
		return;

	FTransform SpawnTransform = GetComponentTransform();
	SpawnedActor = GetWorld()->SpawnActorDeferred<AActor>(ActorToSpawn, SpawnTransform);

	if (!ensure(SpawnedActor))
		return;

	SpawnedActor->AttachToComponent(this, FAttachmentTransformRules::KeepWorldTransform);

	UGameplayStatics::FinishSpawningActor(SpawnedActor, SpawnTransform);
}
