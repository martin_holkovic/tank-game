// Copyright notice

#include "TankPlayerController.h"
#include "TankAimingComponent.h"
#include "GameFramework/Pawn.h"
#include "Camera/CameraComponent.h"
#include "Engine/World.h"
#include "TankPawn.h"


void ATankPlayerController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AimTowardsCrosshair();
}

void ATankPlayerController::SetPawn(APawn* InPawn)
{
	Super::SetPawn(InPawn);

	if (InPawn == nullptr)
		return;

	ATankPawn* tank = Cast<ATankPawn>(InPawn);

	if (!ensure(tank))
		return;

	tank->OnDeath.AddUniqueDynamic(this, &ATankPlayerController::OnTankDeath);
}

void ATankPlayerController::AimTowardsCrosshair()
{
	// not possessing a pawn
	if (GetPawn() == nullptr)
		return;

	FVector HitLocation{ 0 };
	bool bGotHitLocation = GetSightRayHitLocation(HitLocation);

	if (bGotHitLocation)
		GetPawn()->FindComponentByClass<UTankAimingComponent>()->AimAtLocation(HitLocation);
}

bool ATankPlayerController::GetSightRayHitLocation(FVector& OutHitLocation) const
{
	// get starting location of ray cast (from player view port)
	FVector OutFacingDirection{ 0 };
	if (!GetPlayerFacing(OutFacingDirection))
		return false;

	// line trace from camera to the crosshair direction
	return GetPlayerTargetLocation(OutFacingDirection, OutHitLocation);
}

bool ATankPlayerController::GetPlayerFacing(FVector& FacingDirection) const
{
	// find crosshair location
	int32 ViewportSizeX;
	int32 ViewportSizeY;
	GetViewportSize(ViewportSizeX, ViewportSizeY);

	FVector2D ScreenLocation{ ViewportSizeX * CrosshairLocationX, ViewportSizeY * CrosshairLocationY };

	// deproject screen position to absolute location in world
	FVector FacingLocation;
	return DeprojectScreenPositionToWorld(ScreenLocation.X, ScreenLocation.Y, FacingLocation, FacingDirection);
}

bool ATankPlayerController::GetPlayerTargetLocation(FVector FacingDirection, FVector& HitLocation) const
{
	HitLocation = FVector{ 0 };

	FHitResult OutHit;
	FVector LocStart = PlayerCameraManager->GetCameraLocation();
	FVector LocEnd = LocStart + FacingDirection * LineTraceRange;
	if (!GetWorld()->LineTraceSingleByChannel(OutHit, LocStart, LocEnd, ECollisionChannel::ECC_Camera))
		return false;

	HitLocation = OutHit.Location;
	return true;
}

void ATankPlayerController::OnTankDeath()
{
	StartSpectatingOnly();
}

void ATankPlayerController::BeginPlay()
{
	Super::BeginPlay();

	UTankAimingComponent* aimingComponent = GetPawn()->FindComponentByClass<UTankAimingComponent>();
	
	Initialized(aimingComponent);
}
