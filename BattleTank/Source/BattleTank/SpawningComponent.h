// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "Components/SceneComponent.h"
#include "SpawningComponent.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class BATTLETANK_API USpawningComponent : public USceneComponent
{
	GENERATED_BODY()

public:	
	
	USpawningComponent();

	AActor* GetSpawnedActor() const { return SpawnedActor; }

protected:
	
	virtual void BeginPlay() override;

	void SpawnActor();

private:

	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> ActorToSpawn;

	UPROPERTY()
	AActor* SpawnedActor;
};
