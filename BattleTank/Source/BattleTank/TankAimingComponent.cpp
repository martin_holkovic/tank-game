// Copyright notice

#include "TankAimingComponent.h"
#include "GameFramework/Actor.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/World.h"
#include "Projectile.h"

UTankAimingComponent::UTankAimingComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTankAimingComponent::TickComponent(float DeltaTime, enum ELevelTick TickType, FActorComponentTickFunction *ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	double platformTimeSeconds = FPlatformTime::Seconds();
	bool isReloaded = (platformTimeSeconds - LastTimeFired) > ReloadTimeInSeconds;

	if (Ammo == 0)
		AimState = EAimState::OutOfAmmo;
	
	else if (!isReloaded)
		AimState = EAimState::Reloading;
	
	else if (bIsAiming)
		AimState = EAimState::Aiming;
	
	else
		AimState = EAimState::Locked;
}

void UTankAimingComponent::Fire()
{
	if (!ensure(Barrel))
		return;

	if (!ensure(ProjectileBP))
		return;

	double deltaTime = FPlatformTime::Seconds();

	if (AimState == EAimState::Reloading)
		return;

	if (Ammo == 0)
		return;

	// Spawn a projectile at the socket location on the barrel
	FVector SpawnLocation = Barrel->GetSocketLocation(FName("Projectile"));
	FRotator SpawnRotation = Barrel->GetSocketRotation(FName("Projectile"));
	AProjectile* Projectile = GetWorld()->SpawnActor<AProjectile>(ProjectileBP, SpawnLocation, SpawnRotation);

	if (!ensure(Projectile))
		return;

	Projectile->LaunchProjectile(LaunchSpeed);

	LastTimeFired = deltaTime;
	Ammo--;
}

void UTankAimingComponent::Initialize(UTankBarrel * barrelToSet, UTankTurret * turretToSet)
{
	Barrel = barrelToSet;
	Turret = turretToSet;
}

void UTankAimingComponent::AimAtLocation(FVector TargetLocation)
{
	if (!ensure(Barrel))
		return;

	FVector OutLaunchVelocity;
	FVector StartLocation = Barrel->GetSocketLocation(FName("Projectile"));

	if (UGameplayStatics::SuggestProjectileVelocity(this, OutLaunchVelocity, StartLocation, TargetLocation, LaunchSpeed, false, .0f, .0f, ESuggestProjVelocityTraceOption::DoNotTrace, FCollisionResponseParams::DefaultResponseParam, TArray<AActor*>(), false))
	{
		FVector aimDirection = OutLaunchVelocity.GetSafeNormal();
		FVector targetDirection = TargetLocation.GetSafeNormal();

		MoveBarrel(aimDirection);
	}
}

EAimState UTankAimingComponent::GetAimState() const
{
	return AimState;
}

int32 UTankAimingComponent::GetAmmo() const
{
	return Ammo;
}

void UTankAimingComponent::MoveBarrel(FVector direction)
{
	// calculate difference between camera direction and barrel direction
	FRotator BarrelRotation = Barrel->GetForwardVector().Rotation();
	FRotator AimRotation = direction.Rotation();
	FRotator DeltaRotation = AimRotation - BarrelRotation;

	if (!BarrelRotation.Equals(AimRotation, 1.f))
	{
		Barrel->Elevate(DeltaRotation.Pitch);

		if (FMath::Abs(DeltaRotation.Yaw) < 180.0f)
			Turret->Turn(DeltaRotation.Yaw);

		else
			Turret->Turn(-DeltaRotation.Yaw);

		bIsAiming = true;
	}
	else
		bIsAiming = false;
}
