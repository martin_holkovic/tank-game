// Copyright notice

#include "TankPawn.h"

float ATankPawn::TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	if (HitPoints == 0)
		return 0.f;

	float finalDamage = FMath::Clamp(DamageAmount, 0.f, HitPoints);

	HitPoints -= finalDamage;

	if (HitPoints == 0)
	{
		if (OnDeath.IsBound())
			OnDeath.Broadcast();
	}

	return finalDamage;
}

float ATankPawn::GetHealthPercent() const
{
	return HitPoints / MaxHitPoints;
}