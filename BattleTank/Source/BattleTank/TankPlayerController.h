// Copyright notice

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class APawn;

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaTime) override;
	
	// the X coordinate of the crosshair on the widget
	UPROPERTY(EditAnywhere)
	float CrosshairLocationX = 0.5f;

	// the Y coordinate of the crosshair on the widget
	UPROPERTY(EditAnywhere)
	float CrosshairLocationY = 0.33333f;
	
	virtual void SetPawn(APawn* InPawn) override;

protected:

	UFUNCTION(BlueprintImplementableEvent, Category = "Init")
	void Initialized(UTankAimingComponent* aimingComponent);

private:

	float LineTraceRange = 25000.0f;

	float CrosshairPitch = 10.0f;

	void AimTowardsCrosshair();

	bool GetSightRayHitLocation(FVector& OutHitLocation) const;

	bool GetPlayerFacing(FVector& FacingDirection) const;

	bool GetPlayerTargetLocation(FVector FacingDirection, FVector& HitLocation) const;
	
	UFUNCTION()
	void OnTankDeath();
};
