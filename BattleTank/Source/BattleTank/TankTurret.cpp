// Copyright notice

#include "TankTurret.h"
#include "Engine/World.h"

void UTankTurret::Turn(float RelativeSpeed)
{
	RelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1.0f, 1.0f);

	// move the turret the right amount of degrees this frame
	// given the max turn speed and the frame time
	float TurnChange = RelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	float RawNewTurn = RelativeRotation.Yaw + TurnChange;

	SetRelativeRotation(FRotator(0.0f, RawNewTurn, 0.0f));
}