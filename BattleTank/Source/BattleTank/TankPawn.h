// Copyright notice.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "TankPawn.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnTankDeathDelegate);

UCLASS()
class BATTLETANK_API ATankPawn : public APawn
{
	GENERATED_BODY()

public:

	// Returns current health as a % of starting health between 0 and 1
	UFUNCTION(BlueprintPure)
	float GetHealthPercent() const;

	FOnTankDeathDelegate OnDeath;

protected:

	UPROPERTY(EditDefaultsOnly)
	float MaxHitPoints = 1000.f;

	UPROPERTY(EditDefaultsOnly)
	float HitPoints = 1000.f;

private:

	float TakeDamage(float DamageAmount, FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser) override;

};
